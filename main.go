package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

var (
	//INTERVALS predifened candle intervals
	INTERVALS []time.Duration = []time.Duration{5 * time.Minute, 30 * time.Minute, 240 * time.Minute}
)

func main() {
	done := make(chan struct{})
	errors := make([]chan error, 0, len(INTERVALS)*2+1)
	defer close(done)
	inputFile, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer inputFile.Close()
	files := make([]*os.File, 0, len(INTERVALS))
	for _, v := range INTERVALS {
		file, err := os.Create(fmt.Sprintf("candles_%.0fmin.csv", v.Minutes()))
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		files = append(files, file)
	}
	reader, readErrc := ReadTrades(done, inputFile)
	errors = append(errors, readErrc)
	splitted := splitTrades(done, reader, len(INTERVALS))
	for i, v := range INTERVALS {
		candle, candleErrc := candlestickPipeline(done, splitted[i], v)
		errors = append(errors, candleErrc)
		writeErrc := WriteCandles(done, candle, files[i])
		errors = append(errors, writeErrc)
	}
	bl := mergeErrors(errors...)
	for {
		select {
		case err, ok := <-bl:
			if !ok {
				return
			}
			if err != nil {
				log.Fatal(err)
			}
		case <-time.After(time.Second * 5):
			log.Fatal("Process took too long time")
		}
	}

}
