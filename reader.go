package main

import (
	"encoding/csv"
	"io"
)

//ReadTrades reads CSV file using "enconding/csv" with treads an send next to pipeline
func ReadTrades(mainDone <-chan struct{}, file io.Reader) (chan []string, chan error) {
	out := make(chan []string)
	errc := make(chan error)
	go func() {
		defer close(out)
		defer close(errc)
		r := csv.NewReader(file)
		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				errc <- err
				return
			}
			select {
			case out <- record:
			case <-mainDone:
				return
			}
		}
		errc <- nil
	}()
	return out, errc
}
