package main

import (
	"fmt"
	"time"
)

//Candlestick represent a single candle. Timestamp is left border of interval
type Candlestick struct {
	Ticker    string
	Timestamp time.Time
	Open      float64
	MaxPrice  float64
	MinPrice  float64
	Close     float64
}

//CSV return "enconding/csv" compatible csv-record
func (c *Candlestick) CSV() []string {
	return []string{
		c.Ticker,
		c.Timestamp.Format(time.RFC3339),
		fmt.Sprint(c.Open),
		fmt.Sprint(c.MaxPrice),
		fmt.Sprint(c.MinPrice),
		fmt.Sprint(c.Close),
	}
}

func candlestickPipeline(mainDone <-chan struct{}, in <-chan []string, interval time.Duration) (chan Candlestick, chan error) {
	out := make(chan Candlestick, 10)
	errc := make(chan error)
	go processTrades(interval, in, out, errc, mainDone)
	return out, errc
}

func processTrades(interval time.Duration, in <-chan []string, out chan<- Candlestick, errc chan<- error, done <-chan struct{}) {
	defer close(out)
	defer close(errc)

	firstMessage := <-in
	firstTrade, err := ParseTrade(firstMessage)
	if err != nil {
		select {
		case errc <- err:
		default:
		}
		return
	}
	var (
		trade       *Trade = firstTrade
		m           []string
		ok          bool
		localCandle *Candlestick
	)

NEWDAY:
	candleMap := make(map[string]*Candlestick)
	todayStart := time.Date(trade.Timestamp.Year(), time.Month(trade.Timestamp.Month()), trade.Timestamp.Day(), 10, 0, 0, 0, time.UTC)
	todayEnd := todayStart.Add(17 * time.Hour)
	intervalStart := todayStart
	intervalEnd := todayStart.Add(interval)
	for trade.Timestamp.Before(todayStart) {
		//discrad invalid trades
		select {
		case m, ok = <-in:
			if !ok {
				return
			}
		case <-done:
			return
		}
		trade, err = ParseTrade(m)
		if err != nil {
			errc <- err
		}
	}
	for {
		//init map value; The first trade of interval
		if localCandle, ok = candleMap[trade.Ticker]; !ok {
			candleMap[trade.Ticker] = &Candlestick{
				Ticker:    trade.Ticker,
				Timestamp: intervalStart,
				Open:      trade.Price,
				MaxPrice:  trade.Price,
				MinPrice:  trade.Price,
				Close:     trade.Price,
			}
			localCandle = candleMap[trade.Ticker]
		}
		//check timestamp
		if trade.Timestamp.After(todayEnd) {
			for _, v := range candleMap {
				out <- *v
			}
			break
		}
		if trade.Timestamp.After(intervalEnd) {
			//last trade
			// close candles, send all
			for _, v := range candleMap {
				out <- *v
			}
			//Init new candleMap
			candleMap = make(map[string]*Candlestick)
			//Update interval border
			intervalStart = intervalEnd
			intervalEnd = intervalEnd.Add(interval)
			//Process new trade of new interval
			candleMap[trade.Ticker] = &Candlestick{
				Ticker:    trade.Ticker,
				Timestamp: intervalStart,
				Open:      trade.Price,
				MaxPrice:  trade.Price,
				MinPrice:  trade.Price,
				Close:     trade.Price,
			}
		} else {
			localCandle.Close = trade.Price
			//check price
			if trade.Price > localCandle.MaxPrice {
				localCandle.MaxPrice = trade.Price
			} else if trade.Price < localCandle.MinPrice {
				localCandle.MinPrice = trade.Price
			}

		}
		select {
		case m, ok = <-in:
			if !ok {
				out <- *localCandle
				return
			}
		case <-done:
			return
		}
		trade, err = ParseTrade(m)
		if err != nil {
			errc <- err
		}
	}
	goto NEWDAY
}
