package main

import (
	"strconv"
	"time"
)

//Trade is parsed from []string trade record used for candle "drawing"
type Trade struct {
	Ticker    string
	Price     float64
	Timestamp time.Time
}

const (
	inputTimestamp string = "2006-01-02 15:04:05.999999"
)

//ParseTrade converts slice of strings to Trade struct with the right types for next calculations
func ParseTrade(raw []string) (*Trade, error) {
	price, err := strconv.ParseFloat(raw[1], 64)
	if err != nil {
		return nil, err
	}
	// ts, err := time.Parse(RFC3339Micro, raw[3])
	ts, err := time.Parse(inputTimestamp, raw[3])
	if err != nil {
		return nil, err
	}
	return &Trade{
		Ticker:    raw[0],
		Price:     price,
		Timestamp: ts,
	}, nil
}
