package main

import (
	"encoding/csv"
	"io"
)

//WriteCandles write candles to files in CSV format
func WriteCandles(mainDone <-chan struct{}, in <-chan Candlestick, file io.Writer) chan error {
	errc := make(chan error)
	go func() {
		w := csv.NewWriter(file)
		defer w.Flush()
		defer close(errc)
		for {
			select {
			case candle, ok := <-in:
				if !ok {
					return
				}
				err := w.Write(candle.CSV())
				if err != nil {
					errc <- err
					return
				}
			case <-mainDone:
				return
			}
		}
	}()
	return errc
}
